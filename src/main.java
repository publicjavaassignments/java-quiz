import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * GAME FLOW:
 * Definér antal spillere
 * Populer arrays baseret på antal spillere
 * <p>
 * 2 spillere, index 1 i questionCount
 * 3 spillere, index 2 i questionCount
 * <p>
 * Start med første spiller
 * Find tilfældigt spørgsmål, tjek questionAskedArray om spørgsmålet er blevet spurgt før
 * Præsenter spørgsmål, tag spiller input. Hvis rigtigt, giv spilleren 1 point. Hvis forkert, gør intet.
 * Marker spørgsmålet i questionAskedArray med 1 når spørgsmålet er blevet stillet.
 * <p>
 * Fortsæt til næste spiller.
 * <p>
 * Bliv ved indtil der ikke er flere spørgsmål, præsenter scoreboard.
 */

public class main {
    private static final char DEFAULTSEPARATOR = ',';
    private static final char DEFAULTQUOTE = '"';

    public static void main(String[] args) throws FileNotFoundException {
        // Question tracking
        int[] questionCount = {10, 15, 20, 25, 30}; // Question counts (dependent on player count)

        int[] questionIDArray = new int[30]; // Question number
        String[] questionArray = new String[30]; // Question string
        boolean[] answerArray = new boolean[30]; // Answer string

        boolean[] questionAskedArray = new boolean[30]; // Whether or not the question has already been asked

        int currentPlayer = 0;

        // CSV utils
        String csvFile = "questions.csv"; // Relative paths plz lol

        Scanner csvParser = new Scanner(new File(csvFile));

        //Debug line to check array content

        int currentArrayIndex = 0;
        while (csvParser.hasNext()) {

            List<String> line = parseLine(csvParser.nextLine());

            // Temporary conversions
            int temporaryInt = Integer.parseInt(line.get(0));
            boolean temporaryBoolean = Boolean.parseBoolean(line.get(2));

            // Populate arrays
            questionIDArray[currentArrayIndex] = temporaryInt;
            questionArray[currentArrayIndex] = line.get(1);
            answerArray[currentArrayIndex] = temporaryBoolean;

            // Increment array index
            currentArrayIndex++;
        }
        csvParser.close();

        // Player tracking
        System.out.println("How many players are playing?");
        int playerAmount = 0;
        ArrayList<Player> players = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            playerAmount = scanner.nextInt();
            players = createPlayer(playerAmount);
        } else {
            System.out.println("Please enter the amount of players");
        }

        // Actual game loop
        for (int i = 0; i < (questionCount[playerAmount - 2]); ++i) {

            if (currentPlayer == 0) {
                currentPlayer = 0;
            } else {
                currentPlayer = (currentPlayer - 1);
            }

            System.out.println("Player: " + players.get(currentPlayer).getPlayerName() + "'s turn");
            boolean correctAnswer = getQuestion(questionArray, questionAskedArray, answerArray);
            checkAnswer(correctAnswer, players, currentPlayer);

            // Sets the current player, leave for last.
            currentPlayer = ((i % playerAmount) + 1);
        }
        endGameScreen(players);
    }

    public static List<String> parseLine(String csvLine) {
        return parseLine(csvLine, DEFAULTSEPARATOR, DEFAULTQUOTE);
    }

    public static List<String> parseLine(String csvLine, char separators, char customQuote) {

        List<String> result = new ArrayList<>();

        // Check if empty
        if (csvLine == null && csvLine.isEmpty()) {
            return result;
        }

        if (customQuote == ' ') {
            customQuote = DEFAULTQUOTE;
        }

        if (separators == ' ') {
            separators = DEFAULTSEPARATOR;
        }

        StringBuffer curVal = new StringBuffer();
        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;

        char[] chars = csvLine.toCharArray();

        for (char ch : chars) {

            if (inQuotes) {
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                } else {

                    //Allow "" in custom enclosed quote
                    if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true;
                        }
                    } else {
                        curVal.append(ch);
                    }

                }
            } else {
                if (ch == customQuote) {

                    inQuotes = true;

                    //Allow "" in custom enclosed quote
                    if (chars[0] != '"' && customQuote == '\"') {
                        curVal.append('"');
                    }

                    //Double quotes check
                    if (startCollectChar) {
                        curVal.append('"');
                    }

                } else if (ch == separators) {

                    result.add(curVal.toString());

                    curVal = new StringBuffer();
                    startCollectChar = false;

                } else if (ch == '\r') {
                    //ignore LF
                    continue;
                } else if (ch == '\n') {
                    break;
                } else {
                    curVal.append(ch);
                }
            }
        }
        result.add(curVal.toString());

        return result;
    }

    public static ArrayList<Player> createPlayer(int playerAmount) {
        ArrayList<Player> players = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        for (int i = 1; i <= playerAmount; i++) {

            System.out.println("Enter the name of player " + i);
            String playerName = scanner.nextLine();
            Player plr = new Player();
            plr.setPlayerName(playerName);
            plr.setPlayerNumber(i);
            players.add(plr);
        }

        System.out.println();
        System.out.println("Players:");
        for (Player player : players) {
            System.out.println(player.getPlayerName());
        }
        System.out.println();
        return players;
    }

    public static boolean getQuestion(String[] questionArray, boolean[] questionAskArray, boolean[] answerArray) {
        Random rand = new Random();
        int questionNumber = rand.nextInt(questionArray.length);

        if(questionAskArray[questionNumber] == true) {
            questionNumber = rand.nextInt(questionArray.length);
        }

        String question = questionArray[questionNumber];
        System.out.println(question);
        questionAskArray[questionNumber] = true;

        boolean correctAnswer = answerArray[questionNumber];

        return correctAnswer;
    }

    public static void checkAnswer(boolean correctAnswer, ArrayList<Player> players, int currentPlayer) {
        boolean answer;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Write 'yes' or 'no'");
        String userInput;

        while (true) {
            userInput = scanner.nextLine();
            if (userInput.equals("yes")) {
                answer = true;
                break;
            } else if (userInput.equals("no")) {
                answer = false;
                break;
            } else {
                System.out.println("Please answer the question with yes or no.");
            }
        }

        if (answer == correctAnswer) {
            System.out.println("Correct! 1 point added!\n");
            players.get(currentPlayer).setPlayerPoints(players.get(currentPlayer).getPlayerPoints() + 1);
        } else {
            System.out.println("Sorry that answer is incorrect...");
            System.out.println();
        }
    }

    public static void endGameScreen(ArrayList<Player> players) {
        int point = 0;
        String playerName = "";

        for (Player player: players) {

            if(player.getPlayerPoints() > point) {
                point = player.getPlayerPoints();
                playerName = player.getPlayerName();
            }
        }

        System.out.println("//////////////////////We have a winner!\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
        System.out.println("\nFirst place winner with "+ point + " points is... " + playerName + "!");
    }
}