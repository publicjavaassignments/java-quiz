public class Player {

    private int playerPoints;
    private String playerName;
    private int playerNumber;

    public void setPlayerPoints(int points) {
        playerPoints = points;
    }

    public int getPlayerPoints() {
        return playerPoints;
    }

    public void setPlayerName(String name) {
        playerName = name;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerNumber(int number) {
        playerNumber = number;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }
}